---
title: Userspace
weight: 40
---

# Userspace 

RACCOON OS uses the CCSDS [Space Packet Protocol](https://public.ccsds.org/Pubs/133x0b2e1.pdf) and the ESA [Packet Utilisation Standard](https://ecss.nl/standard/ecss-e-st-70-41c-space-engineering-telemetry-and-telecommand-packet-utilization-15-april-2016/).

A number of standard PUS services are available, but it is expected that users will define their own services.

**TODO add some diagrams of the different parts of the userspace**

**TODO describe how we use ROS**
---
title: Application Framework 
---

# Application Framework

The RACCOON application framework is intended to provide a standardized way to develop space applications in RACCOON OS.

Each application processes **only** commands for a single APID.
An application has at least one PUS service.

Multiple applications may process commands for a given APID; for example, if the service implementation is split up in different applications.

# Structure

This section describes what the structure of the source code directory for an application that uses the RACCOON OS app framework should look like.

Currently, only applications written in Rust are supported.

## Rust

```
.
├── Cargo.toml
├── mdb
│   ├── example.xml
│   └── example.py
└── src
    ├── gen
    │   ├── mod.rs
    │   └── example_service.rs
    ├── handler
    │   ├── mod.rs
    │   └── example_service.rs
    └── main.rs
```

- `src/main.rs`: the entry point of the application. Responsible for launching the services and I/O handlers.
- `mdb/example.py` file is a description of a PUS service named `example`.
Running this script causes the following files  to be created:
  - `mdb/example.xml`: a XTCE description of the commands and telemetry in the `example` service.
  - `src/gen/example_service.rs`: a Rust module that contains the descriptions of commands and telemetry, and functions to encode and decode them.
  - `src/handler/example_service.rs`: a skeleton handler for the `example` service.
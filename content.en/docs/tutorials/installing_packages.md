---
title: Installing Packages
weight: 2
---

# Installing Packages

In this tutorial we will learn how to install packages on RACCOON OS.
You should have completed the [emulator]({{<relref "download_and_run">}}) or [hardware machine]({{<relref "download_and_install">}}) in this tutorial before going through this one.

{{<hint "info">}}
This tutorial is **only applicable for development use-cases**.
To add packages to a custom image, see [this](TODO) tutorial.
{{</hint>}}

## Unlock the Filesystem

The RACCOON OS filesystem is managed by [OSTree](TODO) and it is immutable by default (except for `/home` and `/var`, which are writeable).
This is needed for [low bandwidth Delta Updates]({{<relref "delta_updates">}}).

```text
# touch /bin/hello
touch: /bin/hello: Read-only file system
```

During development, you may want to [build an application package](TODO) and directly install it on your running system without needing to [build a system update](TODO) (which takes longer).

We can *unlock* the filesystem, which will create an overlay into which you can install packages (similar to how containers work). OSTree provides two filesystem unlock modes: `transient` (default, doesn't persist across reboots) and `hotfix` (persists across reboots).
We want our changes to persist, so use `hotfix`:

```text
# ostree admin unlock --hotfix
Copying /etc changes: 0 modified, 0 removed, 8 added
Transaction complete; bootconfig swap: yes; bootversion: boot.0.1, deployment count change: 1
Hotfix mode enabled.  A writable overlayfs is now mounted on /usr
for this booted deployment.  A non-hotfixed clone has been created
as the non-default rollback target.
```

Let's inspect the currently active OSTree deployments:

```text
# ostree admin status
* raccoon-os 473726f8ab60d9bab99d9ab266a4baef55f4f70ffc5cef685c6180652ed36fc9.0
    Version: 0.4
    Unlocked: hotfix
    origin refspec: raccoon-os:qemux86-64
  raccoon-os 473726f8ab60d9bab99d9ab266a4baef55f4f70ffc5cef685c6180652ed36fc9.1 (rollback)
    Version: 0.4
    origin refspec: raccoon-os:qemux86-64
```

The current deployment is unlocked, and OSTree has created a rollback deployment.
Reboot and you are now able to add files:

```text
# touch /bin/foo
#
```

{{<hint "warning">}}
**Important**: any changes made to an *unlocked filesystem* will be **DISCARDED** the next time that you [download and deploy an update](TODO). 
This is intended: we want the filesystem to have the exact state that the build system generates.
Files in `/home` and `/var` are preserved across upgrades.
{{</hint>}}

## Refresh the Package Index

Now that the filesystem is unlocked, we can use `opkg` (the package manager on RACCOON OS) to install some of the programs that are available on the package repository.
First, we need to update the package index:

```text
# opkg update
Downloading https://raccoon.jdiez.me/deploy/ipk/all/Packages.gz.
Updated source 'uri-all-0'.
Downloading https://raccoon.jdiez.me/deploy/ipk/core2-64/Packages.gz.
Updated source 'uri-core2-64-0'.
Downloading https://raccoon.jdiez.me/deploy/ipk/qemux86_64/Packages.gz.
Updated source 'uri-qemux86_64-0'.
```

{{<hint "warning">}}
If you run `opkg update` or `opkg install` on a **locked** filesystem, you may encounter unexpected behaviour.
This is a known [issue](TODO).
Make sure to unlock the filesystem as described above before proceeding.
{{</hint>}}

## Install a Package

You can see the list of packages that are compiled by default in the [repo.yml](https://gitlab.com/raccoon-os/raccoon-os/-/blob/master/kas/repo.yml?ref_type=heads) configuration file.

Let's go ahead and install a sane text editor:

```text
# opkg install vim
[...]
Installing vim (9.1.0114) on root
Downloading https://raccoon.jdiez.me/deploy/ipk/core2-64/vim_9.1.0114-r0_core2-64.ipk.
[...]
Configuring vim.
update-alternatives: Linking /usr/bin/vi to /usr/bin/vim.vim
update-alternatives: Linking /usr/bin/vim to /usr/bin/vim.vim
#
```

Now you may want to [install ROS2 and a demo workspace](TODO).
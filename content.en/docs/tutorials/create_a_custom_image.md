---
title: Create a Custom Image
weight: 60
---

# Create a Custom Image

After you have downloaded a pre-built RACCOON OS image and [run it in an emulator]({{<relref "download_and_run" >}}) or [installed it on a supported machine]({{<relref "download_and_install">}}), you may want to create a custom image based on the packages you want to have installed for your deployment/mission. 

Creating a custom image is also a prerequisite for using the [Delta Update](TODO) system.

To do this, you create a new repository which will contain the Yocto build metadata for your deployment.

```text
$ mkdir meta-raccoon-example
$ cd meta-raccoon-example
```

Create a [kas]() `config.yml` file with the following contents:

```yml
header:
  version: 13
  includes:
    - file: kas/preset/qemux86.yml
      repo: raccoon

repos:
  src/raccoon:
    url: https://gitlab.com/raccoon-os/raccoon-os
    branch: master 
    layers:
      meta-raccoon:
      meta-raccoon-bsp:
```

This tells `kas` to clone the RACCOON OS repository, add the `meta-raccooon` and `meta-raccoon-bsp` layers to your build, and also includes the [`kas/preset/qemux86.yml`](https://gitlab.com/raccoon-os/raccoon-os/-/blob/master/kas/preset/qemux86.yml?ref_type=heads) configuration.
You can see which other presets are available in the [Supported Configurations](TODO) page. (`imx8mp`, `qemux86`, `raspberrypi0-wifi`)

These preset configurations are used to build the base development images that are used in the previous tutorials, and are thus a good starting point for your custom build configuration.

## Build Prerequisites

First, install a recent version of `kas`:

```text
$ pip install kas>=4.4
```

You also need to install the build prerequistes listed in the Yocto Project [System Requirements](https://docs.yoctoproject.org/ref-manual/system-requirements.html) page.

It's recommended to install these on your host operating system instead of using a container for the build.
The external dependencies are minimal, and this lets you take the most advantage of the Yocto build system.

However it's also very easy to run any build commands inside a container provided by `kas`. Simply replace `kas` with `kas-container` in any of the following instructions.

## Run the Build

```text
$ kas build config.yml
```

This single command does a lot of work.
- it clones the git repositories of all of the layers and dependencies mentioned in the configuration file
- it creates the bitbake configuration files `$build/conf/local.conf` and `$build/conf/bblayers.conf`
- it sources the OpenEmbedded build environment and
- runs `bitbake $target` (where `$target` is a variable specified in the config.yml - see the [`kas` project configuration documentation](https://kas.readthedocs.io/en/latest/userguide/project-configuration.html)).

This process downloads about 3GB of shared `sstate-cache` files from the RACCOON OS infrastructure.
This is intended to massively speed up builds based on predefined and supported configurations.
On a build machine with 4 threads and a fast internet connection, the build process for this configuration takes approximately 15 minutes.

Some effort has been put to avoid downstream users from having to re-compile the Linux kernel for a supported target, as this is a very CPU-intensive process that can take a long time on non-specialized hardware. 
If you use the preset configurations and don't make any changes that would trigger a kernel recompilation, clean builds should not cause `bitbake` to recompile the kernel.
If this still happens, please [file an issue](https://gitlab.com/raccoon-os/raccoon-os/-/issues).
---
title: Download and Run an Emulator
weight: 1
---

# Download and Run an Emulator

The easiest way to get started with RACCOON OS is to use the [`emu.py`](https://gitlab.com/raccoon-os/raccoon-os/-/blob/master/scripts/emu.py?ref_type=heads) script.
Alternatively, you can [download and install]({{<relref "download_and_install">}}) the OS on a supported machine.

The `emu.py` script downloads the bootloader and filesystem for the image that you choose, creates a backing virtual disk drive, and runs the emulator.
You must have `qemu-system-x86_64` and `python3` installed on your computer.

Create a directory to follow along, download the `emu.py` script and make it executable:

```shell
$ mkdir -p dev/raccoon_hello_world
$ wget https://raccoon.jdiez.me/emu.py
$ chmod +x emu.py
```

Feel free to read the `emu.py` script before executing it.
It's a very simple Python script that doesn't need elevated privileges.
The CI/CD system deploys `emu.py` directly from the git repository.
Let's run it:

```shell
$ ./emu.py -i raccoon-image-dev -p dev-emulator
```

The following things happened:

- the `dev-emulator` folder was created
- the following files were downloaded: `u-boot-qemux86-64.rom` and `raccoon-image-dev-qemux86-64.rootfs.ota-btrfs`
- an `overlay.qcow2` virtual drive was created
- `u-boot` loaded the boot configuration and started the kernel
- the system connected to the virtual network and started a SSH server

The VM's SSH port 22 is forwarded to port 2221 on the host.
So let's connect to it:

```shell
$ ssh -o StrictHostKeyChecking=no root@localhost -p 2221
```

Why do we need `StrictHostKeyChecking=no`?
This is because when you decide to recreate the VM (for example, to test a new OS build), a new SSH host key will be generated - and your SSH client will show a warning about a potential man-in-the-middle attack.

When we connect, we are greeted by a banner reminiscent of installing Slackware from a CD-ROM in the 2000s:

```text
$ ssh -o StrictHostKeyChecking=no root@localhost -p 2221
Welcome to...
______  ___  _____  _____  _____ _____  _____ _   _   _____ _____ 
| ___ \/ _ \/  __ \/  __ \/  __ \  _  ||  _  | \ | | |  _  /  ___|
| |_/ / /_\ \ /  \/| /  \/| /  \/ | | || | | |  \| | | | | \ `--. 
|    /|  _  | |    | |    | |   | | | || | | | . ` | | | | |`--. \
| |\ \| | | | \__/\| \__/\| \__/\ \_/ /\ \_/ / |\  | \ \_/ /\__/ /
\_| \_\_| |_/\____/ \____/ \____/\___/  \___/\_| \_/  \___/\____/ 
                                                                  
version 0.4 - yocto scarthgap - now with 100% more OTA!

root@qemux86-64:/var/rootdirs/home/root# 
```

Let's take a look at the filesystem setup we have out of the box:

```text
root@qemux86-64:/var/rootdirs/home/root# df -kh
Filesystem                Size      Used Available Use% Mounted on
/dev/sda                250.3M    130.2M     54.4M  71% /sysroot
/dev/sda                250.3M    130.2M     54.4M  71% /
/dev/sda                250.3M    130.2M     54.4M  71% /boot
/dev/sda                250.3M    130.2M     54.4M  71% /usr
/dev/sda                250.3M    130.2M     54.4M  71% /var
devtmpfs                470.0M         0    470.0M   0% /dev
tmpfs                   478.0M    128.0K    477.9M   0% /run
```

We can expand it to fill the whole disk:

```text
# btrfs filesystem resize max /
Resize device id 1 (/dev/sda) from 250.25MiB to max
# df -kh
Filesystem                Size      Used Available Use% Mounted on
/dev/sda                 10.0G    130.2M      9.8G   1% /sysroot
/dev/sda                 10.0G    130.2M      9.8G   1% /
[...]
```

You can also [setup data redundancy](TODO) (also known as RAID1) to make the filesystem robust against single bit flips.

What can we do from here?
At the moment, not much.
RACCOON OS ships a versioned, immutable-by-default filesystem.
This is necessary for [low bandwidth Delta Updates]({{< relref "delta_updates" >}}).
Learn about [installing packages]({{<relref "installing_packages" >}}) in the next tutorial.
---
title: Install ROS2 and a Rust Demo Node
---

# Install ROS2 and a Rust Demo Node

In this tutorial we will learn how to install the pre-compiled ROS2 distribution and run a demo node written in Rust.
You must have first completed the setup tutorial for [an emulator]({{<relref "download_and_run">}}) or for a [hardware machine]({{<relref "download_and_install">}}) and unlocked the filesystem in the [Installing Packages]({{<relref "installing_packages">}}) tutorial.

{{<hint "info">}}
If you would like to add the packages discussed in this tutorial to a custom image build, see the [custom image creation](TODO) tutorial.
{{</hint>}}

Since ROS2 plays a crucial role in the [Userspace]({{<relref "userspace">}}), we want to make it as easy as possible to install and run your ROS2 workspaces on RACCOON OS.

You can simply install the ros-core package (remember, your filesystem must be [unlocked]({{<relref "installing_packages">}}#unlock-the-filesystem)):

```text
# opkg install ros-core
Installing rosidl-default-runtime (1.2.0-2) on root
Downloading https://raccoon.jdiez.me/deploy/ipk/core2-64/rosidl-default-runtime_1.2.0-2-r0_core2-64.ipk.
Installing ros-environment (3.2.2-1) on root
Downloading https://raccoon.jdiez.me/deploy/ipk/core2-64/ros-environment_3.2.2-1-r0_core2-64.ipk.
Installing ros-workspace (1.0.2-2) on root
Downloading https://raccoon.jdiez.me/deploy/ipk/core2-64/ros-workspace_1.0.2-2-r0_core2-64.ipk.
Installing sros2-cmake (0.10.5-1) on root
Downloading https://raccoon.jdiez.me/deploy/ipk/core2-64/sros2-cmake_0.10.5-1-r0_core2-64.ipk.
Installing ament-index-python (1.4.0-2) on root
[... many dependencies omitted ...]
Configuring ros-core.
#
```

We can source the ROS2 environment setup script and use the `ros2` cli as usual:

```text
# source /usr/opt/ros/humble/setup.sh 
[... warnings omitted, see below ...]
# ros2 topic list
/parameter_events
/rosout
```

A few things to note here:

- **Instead of the usual base path for ROS2 being `/opt/ros/$ROS_DISTRO`, on RACCOON OS it is `/usr/opt/ros/$DISTRO`**. 
- BusyBox writes some warnings about calling `head` with an invalid option `c`. ROS2 expects to be running on a `sh`-compatible shell, which BusyBox mostly is, but some commandline options to builtin shell functions are not available on BusyBox. This is a [known issue](TODO).
- The `ros2` command takes quite a while to execute. This is a [known issue](TODO) in upstream `ros2-cli`.
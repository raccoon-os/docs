---
title: OS Platform
weight: 10
---

# OS Platform

RACCOON OS consists of the base OS platform and the [Userspace]({{<relref "Userspace">}}).
The base platform is responsible for:

- Booting the system
- Providing commonly used packages like ROS2, Python, GNURadio, etc
- Making it possible to cross-compile and deploy user applications
- Creating a versioned filesystem that can be updated over the air

The base platform is a Linux distrbution created using [Yocto](https://www.yoctoproject.org/).

- 

<!--This was chosen because on ARM processors (which RACCON primarily targets), the bootloader and kernel configuration options are often highly specifc to each device, and vendors often provide a Yocto board support package that takes care of the device-specific details.-->




Eventually, this page will contain information about how to:

- Build a bootable RACCOON OS for a hardware or emulated target
- Add a ROS/RACCOON workspace to the image
- Create an update package
- Enable Secure Boot
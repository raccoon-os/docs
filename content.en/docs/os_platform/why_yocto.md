---
title: Why Yocto Linux?
weight: 1
---

# Why Yocto Linux?

[Yocto Linux (poky)](https://www.yoctoproject.org/) is the base Linux distribution that is used together with the [OpenEmbedded](https://www.openembedded.orgwiki/Main_Page) to build RACCOON OS.
Yocto-based OSes are somewhat different to the typical desktop Linux distributions like Ubuntu, Fedora, Alpine Linux, etc. that you may be familiar with.
These so-called *binary* distributions are very convenient to use.
You can simply download a precompiled disk image that acts as a "live installer", which can be booted in the target system in a standard way.
Once installed, you can use the distribution's package manager to download and install most software that you may typically want to use.

RACCOON OS targets primarily embedded computing systems based on ARM application processors with **very limited resources** like RAM, storage, and processor clock.
This requires some special considerations:

## Boot Process / Linux Kernel

Unlike x86-based systems, whose built-in [BIOS](https://en.wikipedia.org/wiki/BIOS) takes care of initializing the hardware and loading a first-stage bootloader, all of the boot code for ARM computers is stored in user flash. That means that the operating system has to supply the first stage bootloader and any firmware that may be needed for subsystems like RAM. These highly hardware-specific components and their build process are not extensively standarized, so the OS needs to handle them directly.

Thus, most mainstream/binary Linux distributions **don't support booting most ARM-based systems**. There are some notable exceptions like [Raspberry Pi OS](https://www.raspberrypi.com/software/), which is a first-party remix of Debian for Raspberry Pi boards and [Asahi Linux](https://asahilinux.org/), a third-party distribution of Fedora targetting Apple M-series chips. Popular systems like Raspberry Pi tend to get first party support from mainstream distrbutions, but the embedded systems that RACCOON targets would require individiual attention to port these distributions to them.

Furthermore, it is often necessary to apply hardware-specific patches to the Linux kernel for a given target system.
However, we aim to use very similar kernel configurations on supported systems, and run as close to mainline Linux as possible.

And this is where Yocto Linux starts helping us: embedded Linux hardware vendors will almost certainly provide a Board Support Package (BSP) for that particular platform. 
**In theory**, that means that it should be possible to "just add" the vendor's Yocto layer to our build, and we should be able to build the OS from that platform out of the box.
**In reality**, the quality of vendor BSPs varies greatly and may be partially incompatible with our desired approach of deviating as little as possible from mainline Linux and u-boot.
However, in most cases it should be possible to build a working image, and then extract the relevant information from the vendor BSP and reconfigure our build to use more standarized components.
We have already done this process for the [Supported Platforms](TODO_link).

## Cross-Compiling Applications

The next thing one typically wants to do with an embedded system beside installing pre-existing software is to install your own software on it.
Assuming that development computers are x86 based and the target system is ARM based, it is not directly possible to compile software for the target system using the same tools you use to compile on the development computers.

There are a few possibilities here:

- **Copy the source code to the target platform and compile it there**: this is the simplest solution, but your target platform will most likely be significantly less powerful than your development computer, making this option impractical in many cases.
- **Use an ARM build server**: it's possible to rent ARM servers from cloud providers or buy dedicated hardware for this purpose. However, we would like to avoid making that a requirement for building RACCOON OS. 
- **Cross-compile for the target architecture**: takes advantage of your existing development hardware, but introduces significant complexity to the build system.

We choose the third option, cross compiling, considering that most users of RACCOON OS will want to use their existing build computing infrastructure (i.e. even just a laptop running x86 Linux).
Mainstream Linux distribution have **very limited support** for cross-compiling.
